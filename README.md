# README #

# mHealth HelloWorld

## Introduction
This code repository includes the full stack of an mHealth start project from an Android app that collects the
sensor data to the database where it is stored (Parse.com) to a web app that displays the information back to the user.  The data being collected is from an Android phone's accelerometers.  The x, y, and z data is polled and summed every 1/10 of a second.  If sensor values arrive more  frequently than 1/10 of a second, they are averaged together to make one value for that period of time. If the measurement is greater than a set threshold, a reading of "hello" is recorded to the database along with the initial x,y, and z measurements and when it was recorded.  Otherwise, "world" is recorded to the database. The details of this pipeline are displayed below.

##Android app
The source code for the app that records these values is in the mobile_app/Parse-Starter-Project-1.12.0 directory of the repository and will need to be compiled in order to use. All of the important code for this app is in the ParseStartProject/java/com.parse.starter/MainActivity.java file. The code used to record readings
from the app is adapted from the ParseStarterProject available at [Parse.com](http://www.parse.com). The code used to get readings from the accelerometer sensors in the phone was adapted from [this tutorial entitled "Using The Accelerometer on Android"](http://code.tutsplus.com/tutorials/using-the-accelerometer-on-android--mobile-22125).

##Parse
Parse was used in order to avoid writing CRUD functions from scratch and get the full stack of this project up and running as quickly as possible.  The Parse database for this project includes only one table entitled "HelloWorld" which has columns for time recorded, x/y/z values, speed, and type (i.e "hello" or "world").

##Web App
The backend code for the web app is written in python using the Flask framework.  This code does nothing more than serve the page when it is requested. To display the information, there is javascript written inline in the HTML file that is served which polls Parse every second for new data.  This data is added to the DOM dynamically using javascript and formatted using Bootstrap CSS. To host this app I used heroku.com.  The site is available at [mhealth-helloworld.herokuapp.com](https://mhealth-helloworld.herokuapp.com/).

##Putting it all together and running it
Since Parse is all set up and the app is already being hosted on Heroku, all one must do to demonstrate the project is compile and download the app from the mobile_app directory and visit [mhealth-helloworld.herokuapp.com](https://mhealth-helloworld.herokuapp.com/) once the mobile has been started to see the incoming data