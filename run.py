from flask import Flask, render_template
app = Flask(__name__, template_folder='./mhealth/templates/')
app.config['DEBUG'] = True
@app.route('/')
def index():
	return render_template("base.html")

if __name__ == '__main__':
	app.run()