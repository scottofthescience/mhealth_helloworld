/*
 * Copyright (c) 2015-present, Parse, LLC.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */
package com.parse.starter;

import android.content.Context;
import android.hardware.SensorEventListener;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseObject;

import android.app.Activity;
import android.os.Bundle;

import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.starter.R;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.text.ParseException;

/*
Most of this code is adapted from the website at http://code.tutsplus.com/tutorials/using-the-accelerometer-on-android--mobile-22125
for use in the "Wireless and Mobile Health" class at Northwestern University Winter 2016
*/

public class MainActivity extends AppCompatActivity implements SensorEventListener{
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private static int SHAKE_THRESHOLD = 115;

    private EditText  editSensitivity;
    private Button sensitivityButton;
    private TextView sensitivityValLabel;
    private TextView xValLabel;
    private TextView yValLabel;
    private TextView zValLabel;
    private TextView speedValLabel;
    private TextView helloWorldValLabel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ParseAnalytics.trackAppOpenedInBackground(getIntent());


        senSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        sensitivityButton = (Button) findViewById(R.id.sensitivityButton);
        editSensitivity = (EditText) findViewById(R.id.editSensitivity);
        sensitivityValLabel = (TextView) findViewById(R.id.sensitivityValLabel);
        sensitivityValLabel.setText("Sensitivity : " + Float.toString(SHAKE_THRESHOLD));
        xValLabel = (TextView) findViewById(R.id.xValLabel);
        yValLabel = (TextView) findViewById(R.id.yValLabel);
        zValLabel = (TextView) findViewById(R.id.zValLabel);
        speedValLabel = (TextView) findViewById(R.id.speedValLabel);
        helloWorldValLabel = (TextView) findViewById(R.id.helloWorldValLabel);

        sensitivityButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){

                SHAKE_THRESHOLD = Integer.parseInt(editSensitivity.getText().toString());
                sensitivityValLabel.setText("Sensitivity : " + editSensitivity.getText());
            }
        });

    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent){
        Sensor mySensor = sensorEvent.sensor;

        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];
            float z = sensorEvent.values[2];

            long curTime = System.currentTimeMillis();

            if ((curTime - lastUpdate) > 1000) {
                long diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;

                float speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;

                //update UI
                xValLabel.setText("X : " + Float.toString(x));
                yValLabel.setText("Y : " + Float.toString(y));
                zValLabel.setText("Z : " + Float.toString(z));
                speedValLabel.setText("Speed : " + Float.toString(speed));
                ParseObject helloWorld = new ParseObject("HelloWorld");
                if (speed > SHAKE_THRESHOLD){//Send a "Hello"
                    //Log.d("hellomhealth", "Shake : " + speed);
                    helloWorldValLabel.setText("Hello");


                    //"Hello"
                    helloWorld.put("type", "hello");
                    //accelerometer data
                    helloWorld.put("x", x);
                    helloWorld.put("y", y);
                    helloWorld.put("z", z);
                    helloWorld.put("speed", speed);
                    //time
                    helloWorld.put("time", curTime);

                    //location

                } else {// Send a "World"
                    //Log.d("hellomhealth", "not shake : " + speed);
                    helloWorldValLabel.setText("World");

                    //"Hello"
                    helloWorld.put("type", "world");
                    //accelerometer data
                    helloWorld.put("x", x);
                    helloWorld.put("y", y);
                    helloWorld.put("z", z);
                    helloWorld.put("speed", speed);

                    //time
                    helloWorld.put("time", curTime);

                    //location

                }
                helloWorld.saveInBackground(new SaveCallback(){

                    @Override
                    public void done(com.parse.ParseException e) {
                        if (e == null){
                            Log.d("hellomhealth", "Success!");
                        }else{
                            Log.d("hellomhealth", "Fail : " + e.getMessage());
                            Log.d("hellomhealth", "Error Code : " + e.getCode());
                            if (e.getCode() == 209){
                                ParseUser currentUser = ParseUser.getCurrentUser();
                                currentUser.logOut();
                            }
                        }
                    }
                });
                /*
                helloWorld.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            //myObjectSavedSuccessfully();
                        } else {
                            //myObjectSaveDidNotSucceed();
                        }
                    }
                });
                */
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy){
        Log.d("hellomhealth", "onAccuracyChanged()");
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
    return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.action_settings) {
      return true;
    }

    return super.onOptionsItemSelected(item);
    }

    protected void onPause() {
        super.onPause();
        senSensorManager.unregisterListener(this);
    }

    protected void onResume(){
        super.onResume();
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
/*
    public class ParseErrorHandler {
        public static void handleParseError(ParseException e) {
            switch (e.getCode()) {
                case INVALID_SESSION_TOKEN: handleInvalidSessionToken()
                    break;

                 // Other Parse API errors that you want to explicitly handle
            }
        }

        private static void handleInvalidSessionToken() {
            //--------------------------------------
            // Option 1: Show a message asking the user to log out and log back in.
            //--------------------------------------
            // If the user needs to finish what they were doing, they have the opportunity to do so.
            //
            // new AlertDialog.Builder(getActivity())
            //   .setMessage("Session is no longer valid, please log out and log in again.")
            //   .setCancelable(false).setPositiveButton("OK", ...).create().show();

            //--------------------------------------
            // Option #2: Show login screen so user can re-authenticate.
            //--------------------------------------
            // You may want this if the logout button could be inaccessible in the UI.
            //
            // startActivityForResult(new ParseLoginBuilder(getActivity()).build(), 0);
        }
    }

    // In all API requests, call the global error handler, e.g.
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> results, ParseException e) {
                if (e == null) {
                    // Query successful, continue other app logic
                } else {
                    // Query failed
                    ParseErrorHandler.handleParseError(e);
                }
            }
        });
        */
}
